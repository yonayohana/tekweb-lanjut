import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { KategoriComponent } from './pages/kategori/kategori.component';
import { KeranjangComponent } from './pages/keranjang/keranjang.component';

const routes: Routes = [
{
path:'home',
component:HomeComponent
},
{
path:'kategori',
component:KategoriComponent
},
{
path:'keranjang',
component:KeranjangComponent
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
